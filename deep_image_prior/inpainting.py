# %%
from __future__ import print_function
import matplotlib.pyplot as plt
import os
import sys
import numpy as np
import torch
import torch.optim
# sys.path.append('/nas/home/dperini/RIR/deep_image_prior')
from models.resnet import ResNet
from models.unet import UNet
from models.skip import skip
# import pickle
from utils.inpainting_utils import *
from utils.sr_utils import tv_loss, tv_loss_1d

torch.backends.cudnn.enabled = True
torch.backends.cudnn.benchmark = True
dtype = torch.cuda.FloatTensor

PLOT = True
imsize = -1
dim_div_by = 64

num_source = 2
n_mic = 64
pos = "test"
mask_type = 1
TEST_ITERATIONS = True

if num_source == 1:
    source_folder = '1_source'
elif num_source == 2:
    source_folder = '2_sources'
elif num_source == 3:
    source_folder = '3_sources'
elif num_source == 4:
    source_folder = '4_sources'
elif num_source == 5:
    source_folder = '5_sources'

path = os.path.join(pos, source_folder)
path_res = os.path.join(path, 'results_MSE_TV')
img_path = os.path.join(path, 'rir_image.npy')

if mask_type == 1:
    img_mask_np = np.load(os.path.join(path, 'masks/mask_image_1_2.npy'))
elif mask_type == 2:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_3.npy"))
elif mask_type == 3:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_4.npy"))
elif mask_type == 4:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_5.npy"))
elif mask_type == 5:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_6.npy"))
elif mask_type == 6:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_7.npy"))
elif mask_type == 7:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_8.npy"))
elif mask_type == 8:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_9.npy"))
elif mask_type == 9:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_10.npy"))
elif mask_type == 10:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_11.npy"))
elif mask_type == 11:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_12.npy"))
elif mask_type == 12:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_13.npy"))
elif mask_type == 13:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_14.npy"))
elif mask_type == 14:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_15.npy"))
elif mask_type == 15:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_16.npy"))
elif mask_type == 16:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_17.npy"))
elif mask_type == 17:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_18.npy"))
elif mask_type == 18:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_19.npy"))
elif mask_type == 19:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_20.npy"))
elif mask_type == 20:
    img_mask_np = np.load(os.path.join(path, "masks/mask_image_1_21.npy"))

NET_TYPE = 'skip_depth6'  # one of skip_depth4|skip_depth2|UNET|ResNet

# Load mask
img_np = np.load(img_path)
print(img_np.shape)

if TEST_ITERATIONS:
    img_test = img_np
    img_test = np.delete(img_test, slice(0, n_mic-1, mask_type+1), 1).flatten()
'''
# %% Visualize
img_mask_var = np_to_torch(img_mask_np).type(dtype)
# plot_image_grid([img_np, img_mask_np, img_mask_np*img_np], 1,11);
# plt.plot(img_np[0:400, 0])
plt.imshow(img_mask_np[0:100, :], cmap="gray")
plt.show()

# %% Setup
'''
pad = 'reflection'  # 'zero'
OPT_OVER = 'net'
OPTIMIZER = 'adam'

if 'vase.png' in img_path:
    INPUT = 'meshgrid'
    input_depth = 2
    LR = 0.01
    num_iter = 5001
    param_noise = False
    show_every = 50
    figsize = 5
    reg_noise_std = 0.03

    net = skip(input_depth, img_np.shape[0],
               num_channels_down=[128] * 5,
               num_channels_up=[128] * 5,
               num_channels_skip=[0] * 5,
               upsample_mode='nearest', filter_skip_size=1, filter_size_up=3, filter_size_down=3,
               need_sigmoid=True, need_bias=True, pad=pad, act_fun='LeakyReLU').type(dtype)

elif ('rir_image.npy' in img_path) or ('peppers.png' in img_path):
    # Same params and net as in super-resolution and denoising
    INPUT = 'noise'
    input_depth = 32
    LR = 0.01  # learning rate
    # num_iter = 6001
    # num_iter = 301
    num_iter = 1801
    param_noise = False
    show_every = 50
    # figsize = 5
    reg_noise_std = 0.03

    net = skip(input_depth, img_np.shape[0],
               num_channels_down=[128] * 5,
               num_channels_up=[128] * 5,
               num_channels_skip=[128] * 5,
               filter_size_up=3, filter_size_down=3,
               upsample_mode='nearest', filter_skip_size=1,
               # need_sigmoid=True, need_bias=True, pad=pad, act_fun='LeakyReLU').type(dtype)
               need_sigmoid=False, need_bias=True, pad=pad, act_fun='LeakyReLU').type(dtype)

elif 'library.png' in img_path:

    INPUT = 'noise'
    input_depth = 1

    num_iter = 3001
    show_every = 50
    figsize = 8
    reg_noise_std = 0.00
    param_noise = True

    if 'skip' in NET_TYPE:

        depth = int(NET_TYPE[-1])
        net = skip(input_depth, img_np.shape[0],
                   num_channels_down=[16, 32, 64, 128, 128, 128][:depth],
                   num_channels_up=[16, 32, 64, 128, 128, 128][:depth],
                   num_channels_skip=[0, 0, 0, 0, 0, 0][:depth],
                   filter_size_up=3, filter_size_down=5, filter_skip_size=1,
                   upsample_mode='nearest',  # downsample_mode='avg',
                   need1x1_up=False,
                   # need_sigmoid=True, need_bias=True, pad=pad, act_fun='LeakyReLU').type(dtype)
                   need_sigmoid=False, need_bias=True, pad=pad, act_fun = 'LeakyReLU').type(dtype)

        LR = 0.01

    elif NET_TYPE == 'UNET':

        net = UNet(num_input_channels=input_depth, num_output_channels=3,
                   feature_scale=8, more_layers=1,
                   concat_x=False, upsample_mode='deconv',
                   pad='zero', norm_layer=torch.nn.InstanceNorm2d, need_sigmoid=True, need_bias=True)

        LR = 0.001
        param_noise = False

    elif NET_TYPE == 'ResNet':

        net = ResNet(input_depth, img_np.shape[0], 8, 32, need_sigmoid=True, act_fun='LeakyReLU')

        LR = 0.001
        param_noise = False

    else:
        assert False
else:
    assert False

net = net.type(dtype)
# net_input = get_noise(input_depth, INPUT, img_np.shape[1:]).type(dtype)
net_input = get_noise(input_depth, INPUT, img_np.shape).type(dtype)

# Compute number of parameters
s = sum(np.prod(list(p.size())) for p in net.parameters())
print('Number of params: %d' % s)

# Loss
mse = torch.nn.MSELoss().type(dtype)
# L1_norm = torch.nn.L1Loss().type(dtype)
# loss = tf.reduce_sum(tf.image.total_variation(images))
loss_iteration = np.zeros(num_iter)
# conversion from numpy to torch
img_var = np_to_torch(img_np).type(dtype)
mask_var = np_to_torch(img_mask_np).type(dtype)

print(mask_var.shape)
print(img_var.shape)
'''
#%%
# coso = np.array([range(0, 10), range(0, 10), range(0, 10)])
# coso = torch.norm(img_var, p=1, dim=2)

# dh = np.sum(pow((coso[:, 1:] - coso[:, :-1]), 2), axis=1)
# dw = sum(pow((coso[1:, :] - coso[:-1, :]), 2))
# print(dh)

# %% Main loop
'''
i = 0


def closure():
    global i

    if param_noise:
        for n in [x for x in net.parameters() if len(x.size()) == 4]:
            n = n + n.detach().clone().normal_() * n.std() / 50

    net_input = net_input_saved
    if reg_noise_std > 0:
        net_input = net_input_saved + (noise.normal_() * reg_noise_std)

    out = net(net_input)
    mse_loss = mse(out*mask_var, img_var*mask_var)
    # L1_loss = L1_norm(out*mask_var, img_var*mask_var)
    l1_reg = torch.mean(torch.norm(out, p=1, dim=2))
    tv_reg = tv_loss_1d(out, beta=2, dim=2)

    total_loss = 1.0 * mse_loss + 10e-6 * tv_reg + 10e-6 * l1_reg
    total_loss.backward()

    loss_iteration[i] = total_loss.item()
    print('Iteration %05d    Loss %f' % (i, total_loss.item()), '\r', end='')
    if PLOT and i % show_every == 0:
        print(total_loss)
        if TEST_ITERATIONS:
            out = net(net_input).detach().cpu().numpy()[0, 0]
            out_test = np.delete(out, slice(0, n_mic - 1, mask_type+1), 1).flatten()
            error = out_test - img_test
            NMSE = np.dot(error.T, error) / np.dot(img_test.T, img_test)
            print('NMSE = %F    Iteration %05d' %(NMSE, i))
        plt.imshow(out[0:400, :], cmap="gray")
        plt.show()

    i += 1

    return total_loss


net_input_saved = net_input.detach().clone()
noise = net_input.detach().clone()

p = get_params(OPT_OVER, net, net_input)
optimize(OPTIMIZER, p, closure, LR, num_iter)

# %%
out_np = net(net_input).detach().cpu().numpy()[0, 0]
# plot_image_grid([out_np], factor=5);
#plt.imshow(out_np[1:400, :], cmap="gray")
plt.plot(img_np[:, 1])
plt.plot(out_np[:, 1])
plt.show()
print(out_np.shape)
# %% Save results

if mask_type == 1:
    np.save("test/2_sources/results_MSE_TV_L1/-10_-8/rir_m1_1800_dim2", out_np)
    np.save("test/2_sources/results_MSE_TV_L1/-10_-8/loss_m1_1800_nobias", loss_iteration)
elif mask_type == 2:
    np.save("test/1_source/results_MSE_TV_L1/-10_-9/rir_m2_1500_nobias", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-9/loss_m2_1500_nobias", loss_iteration)
elif mask_type == 3:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m3_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m3_1800", loss_iteration)
elif mask_type == 4:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m4_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m4_1800", loss_iteration)
elif mask_type == 5:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m5_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m5_1800", loss_iteration)
elif mask_type == 6:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m6_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m6_1800", loss_iteration)
elif mask_type == 7:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m7_1800_2", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m7_1800_2", loss_iteration)
elif mask_type == 8:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m8_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m8_1800", loss_iteration)
elif mask_type == 9:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m9_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m9_1800", loss_iteration)
elif mask_type == 10:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m10_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m10_1800", loss_iteration)
elif mask_type == 11:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m11_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m11_1800", loss_iteration)
elif mask_type == 12:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m12_1800_3", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m12_1800_3", loss_iteration)
elif mask_type == 13:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m13_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m13_1800", loss_iteration)
elif mask_type == 14:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m14_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m14_1800", loss_iteration)
elif mask_type == 15:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m15_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m15_1800", loss_iteration)
elif mask_type == 16:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m16_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m16_1800", loss_iteration)
elif mask_type == 17:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m17_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m17_1800", loss_iteration)
elif mask_type == 18:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m18_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m18_1800", loss_iteration)
elif mask_type == 19:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m19_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m19_1800", loss_iteration)
elif mask_type == 20:
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/rir_m20_1800", out_np)
    np.save("test/1_source/results_MSE_TV_L1/-10_-8/loss_m20_1800", loss_iteration)
else:
    assert False

# %%
'''
os.system("clear")
del noise
del net_input_saved
torch.cuda.empty_cache()
'''
