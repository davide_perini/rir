from pyroomacoustics import MicrophoneArray, ShoeBox, linear_2D_array
import matplotlib.pyplot as plt
import numpy as np
import os

# ########## PARAMETERS ############
fs_room = 8000
L = 6
W = 5
num_source = 1
array_location = [L / 2, W - 0.5]
pos = "pos_1"
# ########## ROOM ############
# Room params
room_dim = [L, W]
absorption = 0.3
order = 13
# Create shoe box room
room = ShoeBox(room_dim, fs=fs_room, absorption=absorption,
               max_order=order)

# ########## SOURCES ############
# Source 1 position (center)
source_1 = [0.5 * L, 0.3 * W]
# Source 2 position (left)
source_2 = [0.2 * L, 0.2 * W]
# Source 3 position (right)
source_3 = [0.8 * L, 0.2 * W]

# add source(s) to the room
room.add_source(source_1)
source_folder = '1_source'
if num_source == 2:
    room.add_source(source_2)
    source_folder = '2_sources'
if num_source == 3:
    room.add_source(source_2)
    room.add_source(source_3)
    source_folder = '3_sources'

# ########## ARRAY ############
# Linear mic array
n_mic = 64
phi = 0  # counterclockwise rotation of array (from x_axis)
d = 0.02  # distance btw neighboring points
# define the location of the array
R = linear_2D_array(array_location, n_mic, phi, d)
# add array to the room
room.add_microphone_array(MicrophoneArray(R, room.fs))

# ########## PLOT ROOM ############
# room.plot()
# plt.show()

room.compute_rir()

# ########## PROCESSING ON RIRs ############
# Zero padding (all RIRs with the same length)
room_len = [len(rir[0]) for rir in room.rir]
max_len = max(room_len)
room_img = np.array([np.pad(rir[0], (0, max_len - len(rir[0])), "constant", constant_values=0) for rir in room.rir],
                    dtype=float)

# transpose RIRs
room_img = np.transpose(room_img[:, :])

#room_img[0, 0] = 10
# normalization
max_val = np.zeros([n_mic])
for i in range(0, n_mic):
    max_val[i] = np.max(np.abs(room_img[:, i]))
    room_img[:, i] = room_img[:, i] / max_val[i]
# max_val = np.max(np.abs(room_img))
# room_img = room_img / max_val
# remove initial zeros
flag = 0
for i in range(0, len(room_img[:, 0])):
    for j in range(0, len(room_img[0, :])):
        if room_img[i, j] != 0:
            flag = i
            break
    if flag:
        break
room_img = room_img[flag:, :]
# remove final zeros
flag = 0
for i in range(len(room_img[:, 0])-1, 0, -1):
    for j in range(0, len(room_img[0, :])):
        if room_img[i, j] != 0:
            flag = i
            break
    if flag:
        break
room_img = room_img[:flag + 1, :]
# ########## GENERATE MASKS ############
mask_1 = np.zeros(room_img.shape)
mask_2 = np.zeros(room_img.shape)
mask_3 = np.zeros(room_img.shape)
mask_1[:, 1:n_mic:2] = 1
mask_2[:, 1:n_mic:3] = 1
mask_3[:, 1:n_mic:4] = 1

plt.imshow(mask_1[0:20, :], cmap="gray")
plt.show()

# ########## SAVE ############
path = os.path.join(pos, source_folder)
# save RIR and masks
np.save(os.path.join(path, "rir_image"), room_img)
np.save(os.path.join(path, "mask_image_1_2"), mask_1)
np.save(os.path.join(path, "mask_image_1_3"), mask_2)
np.save(os.path.join(path, "mask_image_1_4"), mask_3)
np.save(os.path.join(path, "max_values"), max_val)

