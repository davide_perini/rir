import matplotlib.pyplot as plt
import numpy as np
import os

num_source = 1
pos = "pos_1"

if num_source == 1:
    source_folder = '1_source'
elif num_source == 2:
    source_folder = '2_sources'
else:
    source_folder = '3_sources'

path = os.path.join(pos, source_folder)
path_res = os.path.join(path, "results_MSE")
# convolved_nn = np.load(os.path.join(path_res, "rir_m1_conv_L1_norm.npy"))
convolved_nn = np.load(os.path.join(path_res, "rir_m1_conv_mse1_L1_05.npy"))
# convolved_nn = np.load(os.path.join(path_res, "rir_m1_conv_mse_L1.npy"))
g_truth = np.load(os.path.join(path, "rir_image.npy"))

max_val = np.load(os.path.join(path, "max_values.npy"))

# for i in range(0, len(g_truth[1, :])):
g_truth = g_truth * max_val[:, None].T
convolved_nn = convolved_nn * max_val[:, None].T

# plt.imshow(g_truth[:300, :], cmap="gray")
n_mic = 32
# x = range(600, 800)
plt.plot(g_truth[:, n_mic])
plt.plot(convolved_nn[:, n_mic])
plt.title("Original RIR (blue) vs. reconstructed (orange) \n Microphone #1")
plt.show()

# print(np.amin(room_img))

# mse = np.square(room_img - img_nn).mean(axis=None)
g_t = g_truth.flatten()
c_nn = convolved_nn.flatten()
error = g_t - c_nn

NMSE = np.dot(error.T, error)/np.dot(g_t.T, g_t)
print(NMSE)

# mse = np.square(g_t - c_nn).mean(axis=None)
# print(mse)



