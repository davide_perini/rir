
from pyroomacoustics import MicrophoneArray, ShoeBox, linear_2D_array
import matplotlib.pyplot as plt
import numpy as np
import os
# import scipy.io

# ########## PARAMETERS ############
fs_room = 8000
sound_speed = 343 # (at 20 degree celsius)
L = 6
W = 5
array_location = [L / 2, W - 0.5]

num_sources = 2
pos = "test"
norm = True
comp = True
# ########## ROOM ############
# Room params
room_dim = [L, W]
absorption = 0.3
order = 13
# Create shoe box room
room = ShoeBox(room_dim, fs=fs_room, absorption=absorption,
               max_order=order)

# ########## SOURCES ############
# Source 1 position (center)
source_1 = [0.5 * L, 0.3 * W]
# Source 2 position (left)
source_2 = [0.2 * L, 0.2 * W]
# Source 3 position (right)
source_3 = [0.8 * L, 0.2 * W]
# Source 4 position (middle right)
source_4 = [0.65 * L, 0.15 * W]
# Source 5 position (middle left)
source_5 = [0.35 * L, 0.15 * W]

# add source(s) to the room
room.add_source(source_1)
source_folder = '1_source'
if num_sources == 2:
    room.add_source(source_2)
    source_folder = '2_sources'
elif num_sources == 3:
    room.add_source(source_2)
    room.add_source(source_3)
    source_folder = '3_sources'
elif num_sources == 4:
    room.add_source(source_2)
    room.add_source(source_3)
    room.add_source(source_4)
    source_folder = '4_sources'
elif num_sources == 5:
    room.add_source(source_2)
    room.add_source(source_3)
    room.add_source(source_4)
    room.add_source(source_5)
    source_folder = '5_sources'

# ########## ARRAY ############
# Linear mic array
n_mic = 64
phi = 0  # counterclockwise rotation of array (from x_axis)
d = 0.02  # distance btw neighboring points
# define the location of the array
R = linear_2D_array(array_location, n_mic, phi, d)
# add array to the room
room.add_microphone_array(MicrophoneArray(R, room.fs))

# ########## PLOT ROOM ############
room.plot()
plt.show()

# ########## COMPUTE RIR ############
room.compute_rir()
total_rir = []

max_len = []
for sources in room.rir:
    room_len = [len(rir) for rir in sources]
    max_len = np.append(max_len, max(room_len))

massimo = int(max(max_len))
room_img = np.zeros([massimo, n_mic])
j = 0
for rir in room.rir:
    sum_source_mic = np.sum(np.array([np.pad(rir[i], (0, massimo - len(rir[i])), "constant", constant_values=0) for i in range(0, len(rir))],
                           dtype=float), 0)
    room_img[:, j] = sum_source_mic
    j += 1

compensation = np.zeros([massimo])
for n in range(0, massimo):
    compensation[n] = 4 * np.pi * ((n/fs_room) * sound_speed)

# plt.plot(compensation)
# plt.show()
room_img_save = room_img
# normalization
if comp:
    for i in range(0, n_mic):
        room_img[:, i] = room_img[:, i] * compensation

max_val = np.zeros([n_mic])
if norm:
    for i in range(0, n_mic):
        max_val[i] = np.max(np.abs(room_img[:, i]))
        room_img[:, i] = room_img[:, i] / max_val[i]
# max_val = np.max(np.abs(room_img))
# room_img = room_img / max_val

# remove initial zeros
flag = 0
for i in range(0, len(room_img[:, 0])):
    for j in range(0, len(room_img[0, :])):
        if room_img[i, j] != 0:
            flag = i
            break
    if flag:
        break
room_img = room_img[flag:, :]
room_img_save = room_img_save[flag:, :]
compensation = compensation[flag:]
# remove final zeros
flag = 0
for i in range(len(room_img[:, 0])-1, 0, -1):
    for j in range(0, len(room_img[0, :])):
        if room_img[i, j] != 0:
            flag = i
            break
    if flag:
        break
room_img = room_img[:flag + 1, :]
room_img_save = room_img_save[:flag + 1, :]
compensation = compensation[:flag + 1]
# ########## GENERATE MASKS ############
mask_1 = np.zeros(room_img.shape)
mask_2 = np.zeros(room_img.shape)
mask_3 = np.zeros(room_img.shape)
mask_4 = np.zeros(room_img.shape)
mask_5 = np.zeros(room_img.shape)
mask_6 = np.zeros(room_img.shape)
mask_7 = np.zeros(room_img.shape)
mask_8 = np.zeros(room_img.shape)
mask_9 = np.zeros(room_img.shape)
mask_10 = np.zeros(room_img.shape)
mask_11 = np.zeros(room_img.shape)
mask_12 = np.zeros(room_img.shape)
mask_13 = np.zeros(room_img.shape)
mask_14 = np.zeros(room_img.shape)
mask_15 = np.zeros(room_img.shape)
mask_16 = np.zeros(room_img.shape)
mask_17 = np.zeros(room_img.shape)
mask_18 = np.zeros(room_img.shape)
mask_19 = np.zeros(room_img.shape)
mask_20 = np.zeros(room_img.shape)

mask_1[:, 0:n_mic-1:2] = 1
mask_2[:, 0:n_mic-1:3] = 1
mask_3[:, 0:n_mic-1:4] = 1
mask_4[:, 0:n_mic-1:5] = 1
mask_5[:, 0:n_mic-1:6] = 1
mask_6[:, 0:n_mic-1:7] = 1
mask_7[:, 0:n_mic-1:8] = 1
mask_8[:, 0:n_mic-1:9] = 1
mask_9[:, 0:n_mic-1:10] = 1
mask_10[:, 0:n_mic-1:11] = 1
mask_11[:, 0:n_mic-1:12] = 1
mask_12[:, 0:n_mic-1:13] = 1
mask_13[:, 0:n_mic-1:14] = 1
mask_14[:, 0:n_mic-1:15] = 1
mask_15[:, 0:n_mic-1:16] = 1
mask_16[:, 0:n_mic-1:17] = 1
mask_17[:, 0:n_mic-1:18] = 1
mask_18[:, 0:n_mic-1:19] = 1
mask_19[:, 0:n_mic-1:20] = 1
mask_20[:, 0:n_mic-1:21] = 1


plt.imshow(room_img[0:300, :], cmap="gray")
# plt.plot(room_img[:, 1])
plt.show()
# %%
# ########## SAVE ############
path = os.path.join(pos, source_folder)
# save RIR and masks
np.save(os.path.join(path, "rir_image"), room_img)
np.save(os.path.join(path, "masks/mask_image_1_2"), mask_1)
np.save(os.path.join(path, "masks/mask_image_1_3"), mask_2)
np.save(os.path.join(path, "masks/mask_image_1_4"), mask_3)
np.save(os.path.join(path, "masks/mask_image_1_5"), mask_4)
np.save(os.path.join(path, "masks/mask_image_1_6"), mask_5)
np.save(os.path.join(path, "masks/mask_image_1_7"), mask_6)
np.save(os.path.join(path, "masks/mask_image_1_8"), mask_7)
np.save(os.path.join(path, "masks/mask_image_1_9"), mask_8)
np.save(os.path.join(path, "masks/mask_image_1_10"), mask_9)
np.save(os.path.join(path, "masks/mask_image_1_11"), mask_10)
np.save(os.path.join(path, "masks/mask_image_1_12"), mask_11)
np.save(os.path.join(path, "masks/mask_image_1_13"), mask_12)
np.save(os.path.join(path, "masks/mask_image_1_14"), mask_13)
np.save(os.path.join(path, "masks/mask_image_1_15"), mask_14)
np.save(os.path.join(path, "masks/mask_image_1_16"), mask_15)
np.save(os.path.join(path, "masks/mask_image_1_17"), mask_16)
np.save(os.path.join(path, "masks/mask_image_1_18"), mask_17)
np.save(os.path.join(path, "masks/mask_image_1_19"), mask_18)
np.save(os.path.join(path, "masks/mask_image_1_20"), mask_19)
np.save(os.path.join(path, "masks/mask_image_1_21"), mask_20)

np.save(os.path.join(path, "max_values"), max_val)
np.save(os.path.join(path, "compensation"), compensation)

# scipy.io.savemat(os.path.join(path, "rir_image.mat"), {'room_img': room_img_save, 'mask_1': mask_1})
# scipy.io.savemat(os.path.join(path, "mask_image_1_2.mat"), {'mask_1': mask_1})

# print(sum(room_img_save.flatten()))


''' OLD STUFF

# %%
# ########## PROCESSING ON RIRs ############
# Zero padding (all RIRs with the same length)
room_len = [len(rir[0]) for rir in room.rir]
max_len = max(room_len)
room_img = np.array([np.pad(rir[0], (0, max_len - len(rir[0])), "constant", constant_values=0) for rir in room.rir],
                    dtype=float)
#%%
# transpose RIRs
room_img = np.transpose(room_img[:, :])

# plt.imshow(np.array(room.rir, dtype=float), cmap="gray")
# plt.show()
print(np.shape(room_img))

'''
