
import matplotlib.pyplot as plt
import numpy as np
# from scipy.ndimage import uniform_filter1d
from scipy.signal import resample
import os
n_mic = 64
num_source = 2
pos = "test"
mask_type = "m1"
de_norm = True  # normalization
de_comp = True  # compensation
if num_source == 2:
    source_folder = '1_source'
elif num_source == 2:
    source_folder = '2_sources'
elif num_source == 3:
    source_folder = '3_sources'
elif num_source == 4:
    source_folder = '4_sources'
elif num_source == 5:
    source_folder = '5_sources'


path = os.path.join(pos, source_folder)
path_res = os.path.join(path, "results_MSE_TV_L1/-10_-10")

if mask_type == "m1":
    mask = np.load(os.path.join(path, "masks/mask_image_1_2.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m1_1800_nobias.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m1_1800_nobias.npy"))
elif mask_type == "m2":
    mask = np.load(os.path.join(path, "masks/mask_image_1_3.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m2_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m2_1800.npy"))
elif mask_type == "m3":
    mask = np.load(os.path.join(path, "masks/mask_image_1_4.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m3_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m3_1800.npy"))
elif mask_type == "m4":
    mask = np.load(os.path.join(path, "masks/mask_image_1_5.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m4_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m4_1800.npy"))
elif mask_type == "m5":
    mask = np.load(os.path.join(path, "masks/mask_image_1_6.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m5_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m5_1800.npy"))
elif mask_type == "m6":
    mask = np.load(os.path.join(path, "masks/mask_image_1_7.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m6_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m6_1800.npy"))
elif mask_type == "m7":
    mask = np.load(os.path.join(path, "masks/mask_image_1_8.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m7_1800_2.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m7_1800_2.npy"))
elif mask_type == "m8":
    mask = np.load(os.path.join(path, "masks/mask_image_1_9.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m8_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m8_1800.npy"))
elif mask_type == "m9":
    mask = np.load(os.path.join(path, "masks/mask_image_1_10.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m9_1500.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m9_1800.npy"))
elif mask_type == "m10":
    mask = np.load(os.path.join(path, "masks/mask_image_1_11.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m10_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m10_1800.npy"))
elif mask_type == "m11":
    mask = np.load(os.path.join(path, "masks/mask_image_1_12.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m11_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m11_1800.npy"))
elif mask_type == "m12":
    mask = np.load(os.path.join(path, "masks/mask_image_1_13.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m12_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m12_1800.npy"))
elif mask_type == "m13":
    mask = np.load(os.path.join(path, "masks/mask_image_1_14.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m13_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m13_1800.npy"))
elif mask_type == "m14":
    mask = np.load(os.path.join(path, "masks/mask_image_1_15.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m14_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m14_1800.npy"))
elif mask_type == "m15":
    mask = np.load(os.path.join(path, "masks/mask_image_1_16.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m15_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m15_1800.npy"))
elif mask_type == "m16":
    mask = np.load(os.path.join(path, "masks/mask_image_1_17.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m16_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m16_1800.npy"))
elif mask_type == "m17":
    mask = np.load(os.path.join(path, "masks/mask_image_1_18.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m17_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m17_1800.npy"))
elif mask_type == "m18":
    mask = np.load(os.path.join(path, "masks/mask_image_1_19.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m18_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m18_1800.npy"))
elif mask_type == "m19":
    mask = np.load(os.path.join(path, "masks/mask_image_1_20.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m19_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m19_1800.npy"))
elif mask_type == "m20":
    mask = np.load(os.path.join(path, "masks/mask_image_1_21.npy"))
    convolved_nn = np.load(os.path.join(path_res, "rir_m20_1800.npy"))
    loss_iter = np.load(os.path.join(path_res, "loss_m20_1800.npy"))


g_truth = np.load(os.path.join(path, "rir_image.npy"))
max_val = np.load(os.path.join(path, "max_values.npy"))
# mask = np.load(os.path.join(path, "masks/mask_image_1_2.npy"))
compensation = np.load(os.path.join(path, "compensation.npy"))

# de-normalization
if de_norm:
    g_truth = g_truth * max_val[:, None].T
    convolved_nn = convolved_nn * max_val[:, None].T
# de-compensation
if de_comp:
    g_truth = g_truth / compensation[:, None]
    convolved_nn = convolved_nn / compensation[:, None]

# %%

# plt.imshow(g_truth[:300, :], cmap="gray")
mic_plot = 1
x = range(0, 200)
plt.figure()
plt.plot(g_truth[x, mic_plot])
# plt.plot(convolved_nn[x, mic_plot])
plt.title("Original RIR (blue) vs. reconstructed (orange) \n Microphone #1")
plt.imshow(convolved_nn[x, :], cmap="gray")
plt.show()
# plt.plot(loss_iter[800:])
# plt.title("Loss vs iterations")
# plt.show()

g_t = g_truth
c_nn = convolved_nn


#################
# INTERPOLATION #
#################
# interpolation = uniform_filter1d(g_truth*mask, 3, mode='constant')
# luca = g_truth[:, mask[0, :].astype("bool")], np.shape(mask)
interpolation = resample(g_truth[:, mask[0, :].astype("bool")], np.shape(mask)[1], axis=-1)
# %%
mean_g_truth = np.max(g_truth[:, mask[0, :].astype("bool")], axis=0)
mean_interpolation = np.max(interpolation[:, mask[0, :].astype("bool")], axis=0)
mean = np.mean(mean_g_truth/mean_interpolation)
# print(mean)


interpolation = interpolation * mean

# plt.imshow(interpolation[:100,:], cmap="gray")
# plt.show()
plt.figure()
# plt.plot(g_truth[x, mic_plot])
# plt.plot(interpolation[x, mic_plot])
plt.imshow(interpolation[x, :], cmap="gray")
plt.title("Original RIR (blue) vs. interpolated (orange) \n Microphone #1")
plt.show()

if mask_type == "m1":
    g_t = np.delete(g_t, slice(0, n_mic-1, 2), 1)
    c_nn = np.delete(c_nn, slice(0, n_mic-1, 2), 1)
    interpolation = np.delete(interpolation, slice(0, n_mic-1, 2), 1)
elif mask_type == "m2":
    g_t = np.delete(g_t, slice(0, n_mic - 1, 3), 1)
    c_nn = np.delete(c_nn, slice(0, n_mic - 1, 3), 1)
    interpolation = np.delete(interpolation, slice(0, n_mic - 1, 3), 1)
elif mask_type == "m3":
    g_t = np.delete(g_t, slice(0, n_mic-1, 4), 1)
    c_nn = np.delete(c_nn, slice(0, n_mic-1, 4), 1)
    interpolation = np.delete(interpolation, slice(0, n_mic - 1, 4), 1)
elif mask_type == "m4":
    g_t = np.delete(g_t, slice(0, n_mic-1, 5), 1)
    c_nn = np.delete(c_nn, slice(0, n_mic-1, 5), 1)
    interpolation = np.delete(interpolation, slice(0, n_mic - 1, 5), 1)
elif mask_type == "m5":
    g_t = np.delete(g_t, slice(0, n_mic-1, 6), 1)
    c_nn = np.delete(c_nn, slice(0, n_mic-1, 6), 1)
    interpolation = np.delete(interpolation, slice(0, n_mic - 1, 6), 1)
elif mask_type == "m6":
    g_t = np.delete(g_t, slice(0, n_mic-1, 7), 1)
    c_nn = np.delete(c_nn, slice(0, n_mic-1, 7), 1)
    interpolation = np.delete(interpolation, slice(0, n_mic - 1, 7), 1)
elif mask_type == "m7":
    g_t = np.delete(g_t, slice(0, n_mic-1, 8), 1)
    c_nn = np.delete(c_nn, slice(0, n_mic-1, 8), 1)
    interpolation = np.delete(interpolation, slice(0, n_mic - 1, 8), 1)
elif mask_type == "m8":
    g_t = np.delete(g_t, slice(0, n_mic-1, 9), 1)
    c_nn = np.delete(c_nn, slice(0, n_mic-1, 9), 1)
    interpolation = np.delete(interpolation, slice(0, n_mic - 1, 9), 1)
elif mask_type == "m9":
    g_t = np.delete(g_t, slice(0, n_mic-1, 10), 1)
    c_nn = np.delete(c_nn, slice(0, n_mic-1, 10), 1)
    interpolation = np.delete(interpolation, slice(0, n_mic - 1, 10), 1)
elif mask_type == "m10":
    g_t = np.delete(g_t, slice(0, n_mic-1, 11), 1)
    c_nn = np.delete(c_nn, slice(0, n_mic-1, 11), 1)
    interpolation = np.delete(interpolation, slice(0, n_mic - 1, 11), 1)
# %%
g_t = g_t / g_t.max(axis=0, initial=0)
c_nn = c_nn / c_nn.max(axis=0, initial=0)
interpolation = interpolation / interpolation.max(axis=0, initial=0)

# plt.plot(g_t[:, 0])
# plt.plot(interpolation[:, 0])
# plt.show()

g_t = g_t.flatten()
c_nn = c_nn.flatten()
interp = interpolation.flatten()

# print(np.amin(room_img))

error = g_t - c_nn
error_interp = g_t - interp

NMSE = np.dot(error.T, error)/np.dot(g_t.T, g_t)
NMSE_interp = np.dot(error_interp.T, error_interp)/np.dot(g_t.T, g_t)
print("NMSE NN =", NMSE)
print("NMSE filter =", NMSE_interp)

